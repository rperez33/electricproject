import React,{Component} from 'react';
import Nav from './Nav';
import FullImage from './FullImage';
import bg1jpg from '../../assets/bg-1.jpg';
import Services from './Services';
import Projects from './Projects';
import Footer from './Footer';
import Contacts from './Contacts';
import ModalInformation from './ModalInformation';

class Home extends Component{
	constructor(props){
		super(props);
		this.state = {
			open : false,
			index:null,
			scrollActive : false
		}
	}

	componentDidMount() {
    	window.addEventListener('scroll', this.handleScroll);
  	}


	handleScroll = (event) => {

		let scrollActive = window.scrollY>(window.innerHeight - 80) ? true : false;
    	this.setState({scrollActive});
    
	};

	render(){
		return(
			<div>

				<ModalInformation 
					index = {this.state.index}
					open = {this.state.open}
					handleClose = {()=>this.setState({open:false})}
				/>

				<Nav 
					navActive ={this.state.scrollActive}
				/>

				<FullImage 
					img = {bg1jpg}
				/>
				
				<Services 
					handleMore = {(index)=>this.setState({index,open:true})}
				/>

				<Projects 
					img = {bg1jpg}
				/>

				<Contacts />

				<Footer />

			</div>
		)
	}
}

export default Home;
import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import CallIcon from '@material-ui/icons/Call';
import HomeIcon from '@material-ui/icons/Home';
import ProjectIcon from '@material-ui/icons/VideoLabel';
import ServiceIcon from '@material-ui/icons/Label';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import logo from '../../assets/logo.svg';


const Nav = ({navActive})=>(

	<AppBar position="fixed" style={navActive ? styles.navTrue : styles.navFalse}>
        <Toolbar>
          <Typography variant="h6" color="inherit">
           	<img src = {logo} alt="logo" style={styles.logo}/>
          </Typography>
          <span style={{flex:1}}/>

          <Hidden only="xs">
          	<AnchorLink href="#inicio" style={navActive ? styles.linkTrue : styles.linkFalse}>Inicio</AnchorLink>
          	<AnchorLink href="#services" style={navActive ? styles.linkTrue : styles.linkFalse}>servicios</AnchorLink>
          	<AnchorLink href="#projects" style={navActive ? styles.linkTrue : styles.linkFalse}>portafolio</AnchorLink>
          	<AnchorLink href="#contacts" style={navActive ? styles.linkTrue : styles.linkFalse}>contactanos</AnchorLink>
          </Hidden>
          <Hidden  only={['sm', 'lg','md','xl']}>
          	<AnchorLink href="#inicio"><IconButton  style={navActive ? styles.iconTrue : styles.iconFalse}><HomeIcon /></IconButton></AnchorLink>
          	<AnchorLink href="#services"><IconButton  style={navActive ? styles.iconTrue : styles.iconFalse}><ServiceIcon /></IconButton></AnchorLink>
          	<AnchorLink href="#projects"><IconButton  style={navActive ? styles.iconTrue : styles.iconFalse}><ProjectIcon /></IconButton></AnchorLink>
          	<AnchorLink href="#contacts"><IconButton  style={navActive ? styles.iconTrue : styles.iconFalse}><CallIcon /></IconButton></AnchorLink>
          
          </Hidden>
        </Toolbar>
    </AppBar>
)

const styles = {
	logo:{
		marginTop:10,
		width:35,
		height:35
	},
	navFalse:{
		backgroundColor:'transparent',
		color: 'white',
	},
	navTrue:{
		backgroundColor:'white',
		color: 'black',
	},
	linkFalse:{
		color:'white',
		textDecoration:'none',
		marginLeft:'15px'
	},
	linkTrue:{
		color:'black',
		textDecoration:'none',
		marginLeft:'15px'
	},
	iconFalse:{
		color:'white',
		textDecoration:'none',
		marginLeft:'10px'
	},
	iconTrue:{
		color:'black',
		textDecoration:'none',
		marginLeft:'10px'
	}
}

export default Nav;
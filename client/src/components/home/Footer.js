import React from 'react';

const Footer = ({})=>(

	<div style={styles.ctn}><p>Copyright © 2019 - Todos los derechos reservados</p></div>
)

const styles = {
	ctn:{
		backgroundColor:'white',
		padding:'3em 0',
		display:'flex',
		justifyContent:'center',
		alignItems:'center',
		borderBottom:'6px solid #006e77'
	}
}

export default Footer;
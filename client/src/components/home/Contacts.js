import React from 'react';
import Call from '@material-ui/icons/Call';
import Email from '@material-ui/icons/Email';

const Contacts=({})=>(

	<div style={styles.footer} id="contacts">
		<h1>Contactanos</h1>
		<p>Preguntanos que estamos para servirte!!</p>

		<div style={styles.ctngrid}>
			<div style={styles.ctnIcon}>
				<Call style={styles.icon}/>
          		<p>+584121234567</p>
          	</div>
          	<div style={styles.ctnIcon}>
				<Email style={styles.icon}/>
          		<p>service@companyName.com</p>
          	</div>
		</div>

	</div>

)

const styles={
	footer:{
		display:'flex',
		flexDirection:'column',
		justifyContent:'center',
		alignItems:'center',
		backgroundColor:'#006e77',
		padding: '5rem 0',
		color:'white',
		lineHeight:'2.5em'
	},
	ctngrid:{
		display:'flex',
		margin:'15px 0'
	},

	ctnIcon:{
		display:'flex',
		flexDirection:'column',
		justifyContent:'center',
		alignItems:'center',
		padding:'0 2rem'

	},
	icon:{
		fontSize:'4rem',
		color:'white'
	},
}

export default Contacts;
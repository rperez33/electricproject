import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import Proptypes from 'prop-types';

const FullImage = ({img})=>(
	
	<div style={styles.ctn} id="inicio">
		<h1 style={styles.cWhite}>Nuestro servicios a tu alcance</h1>
		<h3 style={styles.cWhite}>ven y busca lo que necesitas</h3>
		<AnchorLink href="#contacts" style={styles.btn}>Quiero contactarte!!</AnchorLink>
		<img src={img} alt="imagen" style={styles.img}/>
	</div>
	
)

const styles = {
	ctn :{
		position:'relative',
		width:'100%',
		height:window.innerHeight,
		display:'flex',
		flexDirection:'column',
		justifyContent:'center',
		alignItems:'center',

	},
	img:{
		position:'absolute',
		width:'100%',
		height:'100%',
		zIndex:-1
	},
	cWhite:{
		color:'white',
		marginBottom:15
	},
	btn:{
		backgroundColor:'#2b37c3',
		color:'white',
		borderRadius:25,
		paddingTop:10,
		paddingBottom:10,
		paddingLeft:20,
		paddingRight:20,
		cursor:'pointer',
		fontSize:'1.2em',
		boxShadow:'2px 2px 2px rgba(0,0,0,.7)'

	}
}


export default FullImage;
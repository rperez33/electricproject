import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {services} from '../../utils/serviceText'


const ModalInformation = ({open,handleClose,index})=>(

	<Dialog
        onClose={()=>handleClose()}
        aria-labelledby="customized-dialog-title"
        open={open}
    >
        <DialogTitle id="customized-dialog-title" onClose={()=>handleClose()}>
          {services(index).title}
        </DialogTitle>

        <DialogContent>
            <Typography gutterBottom>
              {services(index).article}
            </Typography>
        </DialogContent>
        <DialogActions>
            <Button onClick={()=>handleClose()} color="primary">
              Ok
            </Button>
         </DialogActions>
    </Dialog>

)

export default ModalInformation;
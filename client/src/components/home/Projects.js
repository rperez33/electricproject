import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const Projects = ({img})=>(

	<Grid container spacing={0} style={styles.ctngrid} id="projects">
        <Grid item xs={12} sm={4} style={styles.ctnImg}>
        	<img src={img} alt="imagen" style={styles.img}/>
        </Grid>
        <Grid item xs={12} sm={4} style={styles.ctnImg}>
        	<img src={img} alt="imagen" style={styles.img} />
        </Grid>
        <Grid item xs={12} sm={4} style={styles.ctnImg}>
        	<img src={img} alt="imagen"  style={styles.img}/>	
        </Grid>
        <Grid item xs={12} sm={4} style={styles.ctnImg}>
        	<img src={img} alt="imagen" style={styles.img}/>
        </Grid>
        <Grid item xs={12} sm={4} style={styles.ctnImg}>
        	<img src={img} alt="imagen" style={styles.img} />
        </Grid>
        <Grid item xs={12} sm={4} style={styles.ctnImg}>
        	<img src={img} alt="imagen"  style={styles.img}/>	
        </Grid>
    </Grid>

)

const styles = {
	ctngrid:{
		marginTop:20,
		width:'100%'

	},
	ctnImg:{
		height:'200px'
	},
	img:{
		width:'100%',
		height:'100%'
	}
}


export default Projects;
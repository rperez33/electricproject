import React from 'react';
import Grid from '@material-ui/core/Grid';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import ObjectIcon from '@material-ui/icons/List';
import VisionIcon from '@material-ui/icons/PanTool';
import MisionIcon from '@material-ui/icons/ThumbUp';

const Services =({handleMore})=>(

	<div style={styles.ctn} id="services">
		<h3 style={styles.h3}>¿Quienes somos?</h3>

		<Grid container spacing={24} style={styles.ctngrid}>
        	<Grid item xs={12} sm={4} style={styles.ctnIcon}>
          		<ObjectIcon  style={styles.icon}/>
          		<h2>Objetivos</h2>
          		<p style={styles.note}>Entregar soluciones integrales,que nos permita relacionarse <span style={styles.link} onClick = {()=>handleMore(0)}>...ver mas</span></p>
        	</Grid>
        	<Grid item xs={12} sm={4} style={styles.ctnIcon}>
        		<VisionIcon  style={styles.icon}/>
        		<h2>Vision</h2>
        		<p style={styles.note}>Liderar el mercado de servicios y proyectos electricos a nivel nacional <span style={styles.link} onClick = {()=>handleMore(1)}>...ver mas</span> </p>
        	</Grid>
        	<Grid item xs={12} sm={4} style={styles.ctnIcon}>
        		<MisionIcon  style={styles.icon}/>
        		<h2>Mision</h2>
        		<p style={styles.note}>Entregar soluciones integrales a nuestros clientes del sector electrico <span style={styles.link} onClick = {()=>handleMore(2)}>...ver mas</span> </p>
        	</Grid>

        </Grid>
	</div>

)

const styles = {
	ctn:{
		backgroundColor:'white',
		padding: '8rem 0',
		display:'flex',
		flexDirection:'column',
		justifyContent:'center',
		alignItems:'center',

	},
	h3 : {
		fontSize:'2em'
	},
	ctngrid:{
		marginTop:20,
		width:'100%',
		display:'flex',
		alignItems:'center'

	},
	ctnIcon:{
		display:'flex',
		flexDirection:'column',
		justifyContent:'center',
		alignItems:'center',
		padding:'0 2rem'

	},
	icon:{
		fontSize:'6rem',
		color:'#006e77'
	},
	note:{
		color:'#d7d7d7',
		fontWeight:'bold',
		marginTop:15
	},
	link:{
		color:'#2b37c3',
		cursor:'pointer'
	}
}

export default Services;
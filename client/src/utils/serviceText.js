
const services =  (index)=>{
    
	if(index==0){
        return{
            title : 'Objetivos',
            article:'Entregar soluciones integrales, que nos permita establecer relaciones a largo plazo con nuestros clientes brindandole servicios de alta calidad.'
        }
    }else if(index==1){
        return{
            title : 'Vision',
            article:'Liderar el mercado de servicios y proyectos electricos a nivel nacional,fortaleciendo continuamente nuestra experiencia y calidad,manteniendo el compromiso con nuestros colaboradores y responsabilidad social empresarial para asi asegurar nuestra sustentabilidad como empresa.'
        }
    }else{
        return{
            title : 'Mision',
            article:'Entregar soluciones integrales a nuestros clientes del sector electrico,minero e industrial,aplicando nuestra experiencia,infraestructura,sistema de gestion y personal calificado,cumpliendo altos estandares seguridad,calidad y medio ambiente, a traves de una mejora continua basada en la innovacion y capacitacion permanente,consolidando asi realaciones a largo plazo con nuestros grupos de interes.'
        }
    }
}

export {services};
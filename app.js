var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.set('port',process.env.PORT || 8888);

 if (process.env.NODE_ENV === 'production') {
   
        app.use(express.static(path.join(__dirname,'client', 'build')));
        app.get('/*', (req, res) => {
            res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
        });

    }else{
        app.use(express.static('client/public'));
        app.get('/', (req, res) => {
            res.sendFile(path.resolve(__dirname, 'client', 'public', 'index.html'));
        });
    }

module.exports = app;

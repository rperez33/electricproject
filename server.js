'use strict';
let fs = require('fs');
let app = require('./app');

let server = require('http').createServer(app);

server.listen(app.get('port'), () => {
	console.log('port ' + app.get('port'));
});